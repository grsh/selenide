plugins {
    id("java")
}

repositories {
    mavenCentral()
}

dependencies {
    testImplementation ("com.codeborne:selenide:7.2.3")
    testRuntimeOnly("org.slf4j:slf4j-simple:2.0.12")
    testImplementation(platform("org.junit:junit-bom:5.10.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

tasks.test {
    useJUnitPlatform()
}